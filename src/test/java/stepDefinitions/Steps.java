package stepDefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.hamcrest.Matchers;
import org.hamcrest.core.Is;
import org.json.simple.JSONObject;
import org.junit.Assert;

import static org.hamcrest.core.IsEqual.equalTo;

public class Steps {

    private static final String BASE_URL  = "https://reqres.in/api";
    private Response response;

    private RequestSpecification given() {
        RestAssured.baseURI = BASE_URL;
        return RestAssured.given();
    }

    @Given("Eu faço uma requisição get para o endpoint {string}")
    public void doGetRequest(String endpoint)
    {
        response = given().get(endpoint);
    }

    @Then("Eu recebo uma resposta HTTP {int}")
    public void assertHTTPResponse(int statusCode) {
        Assert.assertEquals(statusCode, response.getStatusCode());
    }

    @Then("E o resultado da requisição deve ter o total de {int} registros")
    public void assertEndpointListUsers(int totalRegistro) {
        response.then()
                .body("$", Matchers.hasKey("page"))
                .body("$", Matchers.hasKey("per_page"))
                .body("$", Matchers.hasKey("total"))
                .body("$", Matchers.hasKey("total_pages"))
                .body("$", Matchers.hasKey("data"))
                .body("data.size()", Is.is(totalRegistro));
    }

    @Then("Verifico o corpo da requisição")
    public void assertEndpointGetUser() {
        response.then()
                .body("$", Matchers.hasKey("data"))
                .body("data", Matchers.hasKey("id"))
                .body("data", Matchers.hasKey("email"))
                .body("data", Matchers.hasKey("first_name"))
                .body("data", Matchers.hasKey("last_name"))
                .body("data", Matchers.hasKey("avatar"))
                .body("data.id", equalTo(1));
    }

    @Given("Eu faço uma requisição post para o endpoint 'users' com os parâmetros name {string} e o parâmetro job {string}")
    public void doPostUserRequest(String name, String job)
    {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("name", name);
        jsonObject.put("job", job);

        response = given()
                .header("Content-type", "application/json")
                .body(jsonObject.toJSONString())
                .post("users");
    }

    @Then("Verifico se o retorno da criação do usuário tem os campos corretos e contem o name {string} e o job {string}")
    public void assertEndpointPostUser(String name, String job) {
        response.then()
                .body("$", Matchers.hasKey("name"))
                .body("$", Matchers.hasKey("job"))
                .body("$", Matchers.hasKey("id"))
                .body("$", Matchers.hasKey("createdAt"))
                .body("name", equalTo(name))
                .body("job", equalTo(job));
    }

    @Given("Eu faço uma requisição patch para o endpoint {string} com o parâmetro job {string}")
    public void doPatchUserRequest(String user, String job)
    {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("job", job);

        response = given()
                .header("Content-type", "application/json")
                .body(jsonObject.toJSONString())
                .patch(user);
    }

    @Then("Verifico se o retorno do patch do usuário tem os campos corretos e contem o job {string}")
    public void assertEndpointPostUser(String job) {
        response.then()
                .body("$", Matchers.hasKey("job"))
                .body("$", Matchers.hasKey("updatedAt"))
                .body("job", equalTo(job));
    }

    @Given("Eu faço uma requisição delete para o endpoint {string}")
    public void doDeleteUserRequest(String user)
    {
        response = given()
                .header("Content-type", "application/json")
                .delete(user);
    }

    @Given("Eu faço uma requisição post para o endpoint 'register' com os valores email {string} e password {string}")
    public void doPostRegisterRequest(String email, String password)
    {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("email", email);
        jsonObject.put("password", password);

        response = given()
                .header("Content-type", "application/json")
                .body(jsonObject.toJSONString())
                .post("register");
    }

    @And("Verifico o retorno do registro do usuário tem os campos corretos")
    public void assertEndpointPostRegister() {
        response.then()
                .body("$", Matchers.hasKey("id"))
                .body("$", Matchers.hasKey("token"));
    }

    @Given("Eu faço uma requisição post para o endpoint 'register' com o valor email {string}")
    public void doPostRegisterRequestInvalid(String email)
    {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("email", email);

        response = given()
                .header("Content-type", "application/json")
                .body(jsonObject.toJSONString())
                .post("register");
    }

    @And("Verifico o retorno do registro do usuário tem o campo error com o valor {string}")
    public void assertEndpointPostRegister(String message) {
        response.then()
                .body("$", Matchers.hasKey("error"))
                .body("error", equalTo(message));
    }

    @Given("Eu faço uma requisição post para o endpoint 'login' com os valores email {string} e password {string}")
    public void doPostLoginRequest(String email, String password)
    {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("email", email);
        jsonObject.put("password", password);

        response = given()
                .header("Content-type", "application/json")
                .body(jsonObject.toJSONString())
                .post("login");
    }

    @And("Verifico o retorno do login do usuário tem os campos corretos")
    public void assertEndpointPostLogin() {
        response.then()
                .body("$", Matchers.hasKey("token"));
    }

    @Given("Eu faço uma requisição post para o endpoint 'login' com o valor email {string}")
    public void doPostLoginRequestInvalid(String email)
    {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("email", email);

        response = given()
                .header("Content-type", "application/json")
                .body(jsonObject.toJSONString())
                .post("login");
    }
}
