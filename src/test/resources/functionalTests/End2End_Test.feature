Feature: End to End teste para api

  Scenario: Consultar lista de usuários
    Given Eu faço uma requisição get para o endpoint 'users'
    Then  Eu recebo uma resposta HTTP 200
    Then  E o resultado da requisição deve ter o total de 6 registros

  Scenario: Consultar um usuário
    Given Eu faço uma requisição get para o endpoint 'users/1'
    Then  Eu recebo uma resposta HTTP 200
    And   Verifico o corpo da requisição

  Scenario: Consultar um usuário inválido
    Given Eu faço uma requisição get para o endpoint 'users/23'
    Then  Eu recebo uma resposta HTTP 404

  Scenario: Criar um usuário
    Given Eu faço uma requisição post para o endpoint 'users' com os parâmetros name "Morpheus" e o parâmetro job "leader"
    Then  Eu recebo uma resposta HTTP 201
    And   Verifico se o retorno da criação do usuário tem os campos corretos e contem o name "Morpheus" e o job "leader"

  Scenario: Atualizar um usuário
    Given Eu faço uma requisição patch para o endpoint 'users/2' com o parâmetro job "baker"
    Then  Eu recebo uma resposta HTTP 200
    And   Verifico se o retorno do patch do usuário tem os campos corretos e contem o job "baker"

  Scenario: Excluir um usuário
    Given Eu faço uma requisição delete para o endpoint 'users/2'
    Then  Eu recebo uma resposta HTTP 204

  Scenario: Registrar um usuário com sucesso
    Given Eu faço uma requisição post para o endpoint 'register' com os valores email "eve.holt@reqres.in" e password "pistol"
    Then  Eu recebo uma resposta HTTP 200
    And   Verifico o retorno do registro do usuário tem os campos corretos

  Scenario: Registrar um usuário sem sucesso
    Given Eu faço uma requisição post para o endpoint 'register' com o valor email "eve.holt@reqres.in"
    Then  Eu recebo uma resposta HTTP 400
    And   Verifico o retorno do registro do usuário tem o campo error com o valor "Missing password"

  Scenario: Realizar o login de um usuário com sucesso
    Given Eu faço uma requisição post para o endpoint 'login' com os valores email "eve.holt@reqres.in" e password "cityslicka"
    Then  Eu recebo uma resposta HTTP 200
    And   Verifico o retorno do login do usuário tem os campos corretos

  Scenario: Realizar o login de um usuário sem sucesso
    Given Eu faço uma requisição post para o endpoint 'login' com o valor email "eve.holt@reqres.in"
    Then  Eu recebo uma resposta HTTP 400
    And   Verifico o retorno do registro do usuário tem o campo error com o valor "Missing password"